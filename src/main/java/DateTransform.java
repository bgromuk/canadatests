import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;
import java.util.stream.Collectors;

public class DateTransform {

    public static final String YYYYMMDD = "yyyymmdd";
    public static final String MM_DD_YYYY = "mm-DD-yyyy";
    public static final String FIRSTSLASHFORMAT = "yyyy/MM/DD";
    public static final String SECONDSLASHFORMAT = "DD/mm/yyyy";

    public static List<String> changeDateFormat(List<String> dates) {
        final SimpleDateFormat sdf = new SimpleDateFormat(YYYYMMDD);
        final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(YYYYMMDD);
        final DateTimeFormatter dashDateFormatter = DateTimeFormatter.ofPattern(MM_DD_YYYY);
        final DateTimeFormatter slashDateFormatter = DateTimeFormatter.ofPattern(FIRSTSLASHFORMAT);
        final DateTimeFormatter secSlashDateFormatter = DateTimeFormatter.ofPattern(SECONDSLASHFORMAT);


        return dates.stream().filter(s -> {
            try {
                sdf.parse(s);
            } catch (ParseException e) {
                return true;
            }
            return false;
        }).map(s -> {
            try {
                return LocalDate.parse(s);
            } catch (DateTimeParseException ex) {
                try {
                    return LocalDate.parse(s, dashDateFormatter);
                } catch (DateTimeParseException ex1) {
                    try {
                        return LocalDate.parse(s, slashDateFormatter);
                    } catch (DateTimeParseException ex2) {
                        return LocalDate.parse(s, secSlashDateFormatter);
                    }
                }
            }
        }).map(localDate -> {
            return localDate.format(formatter);
        }).collect(Collectors.toList());
    }

    public static void main(String[] args) {
        List<String> dates = changeDateFormat(Arrays.asList("2010/03/30", "15/12/2016", "11-15-2012", "20130720"));
        for(String date : dates) {
            System.out.println(date);
        }
    }
}